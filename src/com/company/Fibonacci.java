package com.company;
import java.util.Scanner;

public class Fibonacci {
    public int fib_Cal(int N)
    {

        // Base Case
        if (N <= 1)
        {
            return N;
        }

        // Recursive call
        return fib_Cal(N - 1) + fib_Cal(N - 2);
    }
}
