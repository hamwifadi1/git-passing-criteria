package com.company;
import java.util.Locale;
import java.util.Scanner;

public class Palindrome {
    public void check_Pal()
    {
        //Enter your string to check
        Scanner input=new Scanner(System.in);
        System.out.print("Please enter string : ");
        String str = input.nextLine();
        String reverseStr = "";

        //Check
        for (int i = str.length()-1; i >=0; --i) {
            reverseStr = reverseStr + str.charAt(i);
        }

        if (str.toLowerCase().equals(reverseStr.toLowerCase())) {
            System.out.println(str + " is a Palindrome String.");
        }
        else {
            System.out.println(str + " is not a Palindrome String.");
        }
    }
}
